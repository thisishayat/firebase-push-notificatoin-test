
// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');



    var config = {
        apiKey: "AIzaSyC2sQCyz-BRxJP1mBz3mew0fd5QhvGd01Q",
        authDomain: "amake-dako.firebaseapp.com",
        databaseURL: "https://amake-dako.firebaseio.com",
        projectId: "amake-dako",
        storageBucket: "amake-dako.appspot.com",
        messagingSenderId: "771637915989"
    };
    firebase.initializeApp(config);




// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function(payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    var notificationTitle = 'Background Message Title';
    var notificationOptions = {
        body: 'Background Message body.',
        icon: '/firebase-logo.png'
    };

    return self.registration.showNotification(notificationTitle,
        notificationOptions);
});